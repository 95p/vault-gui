from dataclasses import dataclass
from typing import List


@dataclass
class Vault:
    name: str
    trusted_nodes: List[str]
    storage_nodes: List[str]


@dataclass
class Credential:
    domain: str
    username: str
    password: str


vault = None
credentials = []
proxy_port = None

credentials += [Credential('gmail.com', 'blah', 'blah'),
                Credential('facebook.com', 'blah', 'blah')]
