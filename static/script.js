// execute when document is ready
document.addEventListener("DOMContentLoaded", function(event) {
  // select all the elements with the class of "password"
  showPasswordSetup();
  copyPasswordSetup();
  fetchAllSetup();
  getVault();
});

function showPasswordSetup() {
  const showButtons = document.querySelectorAll(".showpw");
  showButtons.forEach(function(btn) {
    const id = btn.id;
    const pwid = id.replace("showpw-", "pwtext-");
    const pwtext = document.getElementById(pwid).getAttribute("data-password");
    btn.addEventListener("click", function() {
      const pwfield = document.getElementById(pwid);
      const starRepr = pwtext.replace(/./g, "*");
      if (pwfield.innerHTML === starRepr) {
        pwfield.innerHTML = pwtext;
      }
      else {
        pwfield.innerHTML = starRepr;
      }
    });
  });
}


function copyPasswordSetup() {
  const copyButtons = document.querySelectorAll(".pwcopy");
  copyButtons.forEach(function(btn) {
    const content = btn.getAttribute("data-copy");
    // save content to clipboard
    btn.addEventListener("click", function() {
      console.log("copying " + content);
      navigator.clipboard.writeText(content);
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
      console.log(x)
    });
  });
}

function fetchAllSetup() {
  const fetchAll = undefined;
  try {
     fetchAll = document.getElementById("fetchall");
  }
  catch (e) {
    return;
  }
  fetchAll.addEventListener("click", function() {
    console.log("fetching all");
    fetch("/getAll", {method: "POST"})
      .then(function(response) {
    window.location.replace('/');
    });
  });
}


function getVault() {
  const connect = document.querySelectorAll(".connect");
  connect.forEach(function(btn) {
  btn.addEventListener("click", function() {
    console.log("connect vault");
    fetch("/create", {method: "GET"})
      .then(function(response) {
    window.location.replace('/');
    });
  });
  });
}
