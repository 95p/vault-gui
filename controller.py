import model
import requests
import json

NO_BACKEND = False


def get_vault() -> bool:
    res = requests\
            .get(f'http://127.0.0.1:{model.proxy_port}/passworder/getVault')
    vault = json.loads(res.text)
    print(vault)
    model.vault = model.Vault("demo vault",
                              vault['trustedNodes'],
                              [])
    return str(model.proxy_port) in map(lambda x: x.split(':'),
                                        vault['trustedNodes'])


def add_credential(cred: model.Credential) -> bool:
    if NO_BACKEND:
        return True
    endpoint = f'http://127.0.0.1:{model.proxy_port}/passworder/savepwd'
    data = {
        'Domain': cred.domain,
        'Username': cred.username,
        'Password': cred.password
            }
    res = requests.post(endpoint, data=json.dumps(data))
    return res.status_code == 200


def use_existing_vault(name: str) -> bool:
    pass


def get(domain: str) -> bool:
    if NO_BACKEND:
        credential = model.Credential(domain, 'username', 'password')
        model.credentials.append(credential)
        return True
    endpoint = f'http://127.0.0.1:{model.proxy_port}/passworder/getpwd'
    res = requests.get(endpoint, params={'domain': domain})
    print(res.text)
    return True


def get_all() -> bool:
    if NO_BACKEND:
        credential = model.Credential('remoteDomain', 'username', 'password')
        model.credentials.append(credential)
        return True
    else:
        pass
