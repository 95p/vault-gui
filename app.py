from flask import Flask, render_template, request, redirect, url_for
import model
import hashlib
import controller
import os

# read environment variable PROXY_PORT
proxy_port = int(os.environ['PROXY_PORT'])
print('proxy_port: ', proxy_port)
model.proxy_port = proxy_port

app = Flask(__name__)


def hashit(s: str) -> str:
    """
    used for creating unique ids for passwords, in order to find an show them
    with the eye button
    """
    return hashlib.sha1(s.encode()).hexdigest()


def star_repr(s: str) -> str:
    return '*' * len(s)


app.jinja_env.globals.update(hashit=hashit)
app.jinja_env.globals.update(star_repr=star_repr)


@app.route("/")
def application():
    print(model.vault)
    if not model.vault:
        return render_template('create.html', address=str(proxy_port))
    return render_template('vault.html',
                           credentials=model.credentials,
                           retrieved=[])


@app.route('/create', methods=['GET'])
def create():
    controller.get_vault()
    return redirect(url_for('application'))


@app.route("/add", methods=['POST'])
def add():
    cred = model.Credential(domain=request.form['addDomain'],
                            username=request.form['addUsername'],
                            password=request.form['addPassword'])
    print(cred)
    model.credentials.append(cred)
    if controller.add_credential(cred):
        return redirect(url_for('application'))
    else:
        return "error"


@app.route("/", methods=['POST'])
def get():
    domain = request.form['getDomain']
    if controller.get(domain):
        return render_template('vault.html',
                               credentials=model.credentials,
                               retrieved=model.credentials[-1:])
    else:
        return "error"


@app.route("/getAll", methods=['POST'])
def get_all():
    print('get_all')
    if controller.get_all():
        return render_template('vault.html',
                               credentials=model.credentials,
                               retrieved=[])
    else:
        return "error"
